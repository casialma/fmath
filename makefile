#!/usr/bin/make


# defaults
STATIC = yes
COMPILER = gnu

#main building variables
DOBJ = build/obj/
DMOD = build/mod/
DEXE = bin/
DLIB = lib/
DSRC = src/
ifeq "$(STATIC)" "yes"
  MAKELIB = ar -rcs $(DLIB)libfmath.a $(DOBJ)*.o $(DMOD)*.mod
  RULE    = libfmath
else
  RULE = $(DEXE)test1 $(DEXE)test2
  ARX = $(DLIB)libfmath.a
  ARX += $(DLIB)libfio.a
endif
LIBSEXT = $(DLIB)libfmath.a
LIBSEXT += $(DLIB)libfio.a
LIBSEXT += $(DLIB)liblapack.a
LIBSEXT += $(DLIB)librefblas.a
LIBSEXT += $(DLIB)libspecialfunctions.a
ifeq "$(COMPILER)" "gnu"
  FC    = gfortran
  OPTSC = -cpp -c -frealloc-lhs -O2 -J $(DMOD)
  OPTSL = -J $(DMOD)
endif
VPATH   = $(DSRC) $(DOBJ) $(DMOD)
MKDIRS  = $(DOBJ) $(DMOD) $(DEXE) $(DLIB)
LCEXES  = $(shell echo $(EXES) | tr '[:upper:]' '[:lower:]')
EXESPO  = $(addsuffix .o,$(LCEXES))
EXESOBJ = $(addprefix $(DOBJ),$(EXESPO))

#auxiliary variables
COTEXT = "Compile $(<F)"
LITEXT = "Assemble $@"
RUTEXT = "Executed rule $@"

firsrule: $(RULE)

#building rules
$(DEXE)test1: $(MKDIRS) $(ARX) $(DOBJ)test1.o
	@rm -f $(filter-out $(DOBJ)test1.o,$(EXESOBJ))
	@echo $(LITEXT)
	@$(FC) $(OPTSL) $(DOBJ)*.o $(LIBSEXT) -o $@
EXES := $(EXES) test1

$(DEXE)test2: $(MKDIRS) $(ARX) $(DOBJ)test2.o
	@rm -f $(filter-out $(DOBJ)test2.o,$(EXESOBJ))
	@echo $(LITEXT)
	@$(FC) $(OPTSL) $(DOBJ)*.o $(LIBSEXT) -o $@
EXES := $(EXES) test2

libfmath: $(MKDIRS) $(DOBJ)fmath.o
	@echo $(LITEXT)
	@$(MAKELIB)
LIBS := $(LIBS) libfmath.a

#compiling rules
$(DOBJ)fmath_algebra_complex_eigen.o: src/lib/algebra/complex_eigen.f90 \
	$(DOBJ)fmath_lapack.o \
	$(DOBJ)fmath_algebra_complex_matrix.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)fmath_algebra_complex_matrix.o: src/lib/algebra/complex_matrix.f90 \
	$(DOBJ)fio_text_file.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)fmath_algebra_inverse.o: src/lib/algebra/inverse.f90 \
	$(DOBJ)fmath_lapack.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)fmath_lapack.o: src/lib/algebra/malapack.f90
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)fmath_algebra_real_eigen.o: src/lib/algebra/real_eigen.f90 \
	$(DOBJ)fmath_lapack.o \
	$(DOBJ)fmath_algebra_complex_matrix.o \
	$(DOBJ)fmath_algebra_real_matrix.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)fmath_algebra_real_matrix.o: src/lib/algebra/real_matrix.f90 \
	$(DOBJ)fio_text_file.o \
	$(DOBJ)fmath_algebra_inverse.o \
	$(DOBJ)fmath_algebra_solve.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)fmath_algebra_solve.o: src/lib/algebra/solve.f90 \
	$(DOBJ)fmath_lapack.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@
	
$(DOBJ)fmath_special_functions.o: src/lib/special_functions/special_functions.f90
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)fmath.o: src/lib/fmath.f90 \
	$(DOBJ)fmath_algebra_complex_eigen.o \
	$(DOBJ)fmath_algebra_complex_matrix.o \
	$(DOBJ)fmath_algebra_real_eigen.o \
	$(DOBJ)fmath_algebra_real_matrix.o \
	$(DOBJ)fmath_special_functions.o
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)test1.o: src/tests/test1.f90
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)test2.o: src/tests/test2.f90
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@


#phony auxiliary rules
.PHONY : $(MKDIRS)
$(MKDIRS):
	@mkdir -p $@
.PHONY : $(ARX)
$(ARX):
	@ar -x $@
	@rm -f *.o
	@mv *.mod $(DMOD)
.PHONY : cleanobj
cleanobj:
	@echo deleting objects
	@rm -fr $(DOBJ)
.PHONY : cleanmod
cleanmod:
	@echo deleting mods
	@rm -fr $(DMOD)
.PHONY : cleanexe
cleanexe:
	@echo deleting exes
	@rm -f $(addprefix $(DEXE),$(EXES))
cleanlib:
	@echo deleting libs
	@rm -f $(addprefix $(DLIB),$(LIBS))
.PHONY : clean
clean: cleanobj cleanmod
.PHONY : cleanall
cleanall: clean cleanexe cleanlib
