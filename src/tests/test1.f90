!
! Copyright (c) 2016 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
!****************************************************************************
!
! FILE: test1.f90
!
! AUTHOR: Casimiro Alvarez Mariño.
!
! SUBROUTINE: test1
!
! PURPOSE:  realiza el test de la clase formath_special_functions_class.
!
! CREATED DATE: 2017/03/31
!
! MODIFIED DATE: 2017/03/31
!
!****************************************************************************
!
program test1
    use fmath_special_functions_c
    implicit none
    type(fmath_special_functions_type) :: obj
    real(kind=8) :: res
    complex*16 :: res1
    
    obj = fmath_special_functions_new()
    
    res = obj%elliptic_K(0.5d0)
    write(*,*) res
    res = obj%elliptic_E(0.5d0)
    write(*,*) res
    
    res = obj%modified_struve_L0(0.5d0)
    write(*,*) res
    res = obj%modified_struve_L1(0.5d0)
    write(*,*) res
    
    res = obj%modified_bessel_i0(1.0d0)
    write(*,*) res
    res = obj%modified_bessel_k0(1.0d0)
    write(*,*) res
    res = obj%modified_bessel_i1(1.0d0)
    write(*,*) res
    res = obj%modified_bessel_k1(1.0d0)
    write(*,*) res
    
    res1 = obj%modified_bessel_i0((1.0d0,1.0d0))
    write(*,*) res
    res1 = obj%modified_bessel_k0((1.0d0,1.0d0))
    write(*,*) res
    res1 = obj%modified_bessel_i1((1.0d0,1.0d0))
    write(*,*) res
    res1 = obj%modified_bessel_k1((1.0d0,1.0d0))
    write(*,*) res
    

    stop
end program test1
