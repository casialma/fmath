program test2
    use fmath_algebra_real_matrix_c
    implicit none
    integer :: i, j
    type(fmath_algebra_real_matrix_type) :: a

    a = new_fmath_algebra_real_matrix(2)

    call a%set_item(1, 1, 1.0)
    call a%set_item(2, 2, 2.0)

    do i = 1, a%get_num_rows()
        do j = 1, a%get_num_columns() - 1
            write(*,"(ES13.6)", ADVANCE = 'no') a%get_item(i,j)
        enddo
        write(*,"(ES13.6)", ADVANCE = 'yes') a%get_item(i,j)
    enddo


    stop
end program test2
