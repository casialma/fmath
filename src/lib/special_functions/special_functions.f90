!
! Copyright (c) 2016 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
!****************************************************************************
!
! FILE: special_functions.f90
!
! AUTHOR: Casimiro Alvarez Mariño.
!
! CLASS: fmath_special_functions_c
!
! PURPOSE:  clase que define funciones especiales.
!
! CREATED DATE: 2017/03/31
!
! MODIFIED DATE: 2017/03/31
!
!****************************************************************************
!
module fmath_special_functions_c
    implicit none
    private
    public :: fmath_special_functions_type
    public :: fmath_special_functions_new
    public :: fmath_special_functions_DBESIK01
    public :: fmath_special_functions_ZBESIK01

    type :: fmath_special_functions_type
    contains
        procedure, public, pass :: elliptic_KE => fmath_special_functions_elliptic_KE
        procedure, public, pass :: elliptic_K => fmath_special_functions_elliptic_K
        procedure, public, pass :: elliptic_E => fmath_special_functions_elliptic_E
        procedure, public, pass :: modified_struve_L0 => fmath_special_functions_modified_struve_L0
        procedure, public, pass :: modified_struve_L1 => fmath_special_functions_modified_struve_L1
        generic, public :: modified_bessel_I0 => fmath_special_functions_bessel_I0_double, &
                                                fmath_special_functions_bessel_I0_complex
        generic, public :: modified_bessel_I1 => fmath_special_functions_bessel_I1_double, &
                                                fmath_special_functions_bessel_I1_complex
        generic, public :: modified_bessel_K0 => fmath_special_functions_bessel_K0_double, &
                                                fmath_special_functions_bessel_K0_complex
        generic, public :: modified_bessel_K1 => fmath_special_functions_bessel_K1_double, &
                                                fmath_special_functions_bessel_K1_complex

        procedure, private, pass :: fmath_special_functions_bessel_I0_double
        procedure, private, pass :: fmath_special_functions_bessel_I0_complex
        procedure, private, pass :: fmath_special_functions_bessel_I1_double
        procedure, private, pass :: fmath_special_functions_bessel_I1_complex
        procedure, private, pass :: fmath_special_functions_bessel_K0_double
        procedure, private, pass :: fmath_special_functions_bessel_K0_complex
        procedure, private, pass :: fmath_special_functions_bessel_K1_double
        procedure, private, pass :: fmath_special_functions_bessel_K1_complex
    end type

    interface fmath_special_functions_new
        module procedure :: fmath_special_functions_new1
    end interface

contains

    function fmath_special_functions_new1() result(this)
        type(fmath_special_functions_type) :: this
    end function

    function fmath_special_functions_bessel_I0_double(self, x) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        double precision, intent(in) :: x
        double precision :: res
        double precision :: i0, i1, k0, k1

        call fmath_special_functions_DBESIK01(x, i0, i1, k0, k1)
        res = i0
    end function

    function fmath_special_functions_bessel_I0_complex(self, z) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        complex*16, intent(in) :: z
        complex*16 :: res
        complex*16 :: i0, i1, k0, k1

        call fmath_special_functions_ZBESIK01(z, i0, i1, k0, k1)
        res = i0
    end function

    function fmath_special_functions_bessel_I1_double(self, x) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        double precision, intent(in) :: x
        double precision :: res
        double precision :: i0, i1, k0, k1

        call fmath_special_functions_DBESIK01(x, i0, i1, k0, k1)
        res = i1
    end function

    function fmath_special_functions_bessel_I1_complex(self, z) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        complex*16, intent(in) :: z
        complex*16 :: res
        complex*16 :: i0, i1, k0, k1

        call fmath_special_functions_ZBESIK01(z, i0, i1, k0, k1)
        res = i1
    end function

    function fmath_special_functions_bessel_K0_double(self, x) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        double precision, intent(in) :: x
        double precision :: res
        double precision :: i0, i1, k0, k1

        call fmath_special_functions_DBESIK01(x, i0, i1, k0, k1)
        res = k0
    end function

    function fmath_special_functions_bessel_K0_complex(self, z) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        complex*16, intent(in) :: z
        complex*16 :: res
        complex*16 :: i0, i1, k0, k1

        call fmath_special_functions_ZBESIK01(z, i0, i1, k0, k1)
        res = k0
    end function

    function fmath_special_functions_bessel_K1_double(self, x) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        double precision, intent(in) :: x
        double precision :: res
        double precision :: i0, i1, k0, k1

        call fmath_special_functions_DBESIK01(x, i0, i1, k0, k1)
        res = k1
    end function

    function fmath_special_functions_bessel_K1_complex(self, z) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        complex*16, intent(in) :: z
        complex*16 :: res
        complex*16 :: i0, i1, k0, k1

        call fmath_special_functions_ZBESIK01(z, i0, i1, k0, k1)
        res = k1
    end function

    ! modified struve functions
    function fmath_special_functions_modified_struve_L0(self, x) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        double precision, intent(in) :: x
        double precision :: res
        interface
            subroutine stvl0(x, sl0)
                real*8 x
                real*8 sl0
            end subroutine stvl0
        end interface

        call stvl0(x, res)
    end function

    function fmath_special_functions_modified_struve_L1(self, x) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        double precision, intent(in) :: x
        double precision :: res
        interface
            subroutine stvl1(x, sl1)
                real*8 x
                real*8 sl1
            end subroutine stvl1
        end interface

        call stvl1(x, res)
    end function

    ! complete elliptic integrals
    function fmath_special_functions_elliptic_KE(self, x) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        double precision, intent(in) :: x
        double precision :: res(2)
        double precision :: k, e

        call fmath_special_functions_COMELP(x, k, e)
        res(1) = k
        res(2) = e
    end function

    function fmath_special_functions_elliptic_K(self, x) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        double precision, intent(in) :: x
        double precision :: res
        double precision :: k, e

        call fmath_special_functions_COMELP(x, k, e)
        res = k
    end function

    function fmath_special_functions_elliptic_E(self, x) result(res)
        class(fmath_special_functions_type), intent(in) :: self
        double precision, intent(in) :: x
        double precision :: res
        double precision :: k, e

        call fmath_special_functions_COMELP(x, k, e)
        res = e
    end function

    subroutine fmath_special_functions_COMELP(m, k, e)
        interface
            subroutine comelp ( hk, ck, ce )
                real*8 hk
                real*8 ck, ce
            end subroutine comelp
        end interface
        double precision, intent(in) :: m
        double precision, intent(inout) :: k, e

        k = 0.0d0
        e = 0.0d0
        if ((-1 .le. m) .and. (m .le. 1)) then
            call comelp(m, k, e)
        else
            write(*,*) 'error in ellipke: modulus must be -1 <= m <= 1'
            stop
        endif
        return
    end subroutine

    subroutine fmath_special_functions_DBESIK01(x, i0, i1, k0, k1)
        interface
            subroutine ik01a ( x, bi0, di0, bi1, di1, bk0, dk0, bk1, dk1 )
                real*8 x
                real*8 bi0, di0, bi1, di1, bk0, dk0, bk1, dk1
            end subroutine ik01a
            subroutine ik01b ( x, bi0, di0, bi1, di1, bk0, dk0, bk1, dk1 )
                real*8 x
                real*8 bi0, di0, bi1, di1, bk0, dk0, bk1, dk1
            end subroutine ik01b
        end interface
        real*8, intent(in) :: x
        real*8, intent(inout) :: i0, i1, k0, k1
        real*8 bi0, di0, bi1, di1, bk0, dk0, bk1, dk1

        call ik01a(x, bi0, di0, bi1, di1, bk0, dk0, bk1, dk1)
        !call ik01b(x, bi0, di0, bi1, di1, bk0, dk0, bk1, dk1)
        i0=bi0
        i1=bi1
        k0=bk0
        k1=bk1
        return
    end subroutine

    subroutine fmath_special_functions_ZBESIK01(z, cbi0, cbi1, cbk0, cbk1)
        interface
            subroutine cik01 (z, cbi0, cdi0, cbi1, cdi1, cbk0, cdk0, cbk1, cdk1)
                complex*16 z
                complex*16 cbi0, cdi0, cbi1, cdi1, cbk0, cdk0, cbk1, cdk1
            end subroutine cik01
        end interface
        complex*16, intent(in) :: z
        complex*16, intent(inout) :: cbi0, cbi1, cbk0, cbk1
        complex*16 :: cdi0, cdi1, cdk0, cdk1

        cbi0=(0.0,0.0)
        cdi0=(0.0,0.0)
        cbi1=(0.0,0.0)
        cdi1=(0.0,0.0)
        cbk0=(0.0,0.0)
        cdk0=(0.0,0.0)
        cbk1=(0.0,0.0)
        cdk1=(0.0,0.0)
        call cik01(z, cbi0, cdi0, cbi1, cdi1, cbk0, cdk0, cbk1, cdk1)
        return
    end subroutine
end module
