!
! Copyright (c) 2017 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
!****************************************************************************
!
! FILE: fmath.f90
!
! AUTHOR: Casimiro Alvarez Mariño.
!
! CLASS: fmath
!
! PURPOSE: clase que define la librería fmath.
!
! CREATED DATE: 2017/05/11
! MODIFIED DATE: 2017/05/11
!
!****************************************************************************
!
module fmath_c
    use fmath_algebra_complex_eigen_c
    use fmath_algebra_complex_matrix_c
    use fmath_algebra_real_eigen_c
    use fmath_algebra_real_matrix_c
    use fmath_special_functions_c
    implicit none
    public
end module
