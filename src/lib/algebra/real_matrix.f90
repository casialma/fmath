!
! Copyright (c) 2017 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
!****************************************************************************
!
! FILE: real_matrix.f90
!
! AUTHOR: Casimiro Alvarez Mariño.
!
! CLASS: fmath_algebra_real_matrix_c
!
! PURPOSE: clase que define matrices reales de dos dimensiones.
!
! CREATED DATE: 2017/05/09
! MODIFIED DATE: 2017/06/01
!
!****************************************************************************
!
module fmath_algebra_real_matrix_c
    implicit none
    private
    public :: fmath_algebra_real_matrix_type, new_fmath_algebra_real_matrix
    public :: assignment (=)
    public :: operator (+)
    public :: operator (-)
    public :: operator (*)

    type fmath_algebra_real_matrix_type
        integer :: n_rows
        integer :: n_cols
        real, dimension(:,:), allocatable :: storage
    contains
        procedure, public, pass :: get_num_rows => fmath_algebra_real_matrix_get_num_rows
        procedure, public, pass :: get_num_columns => fmath_algebra_real_matrix_get_num_columns
        procedure, public, pass :: get_item => fmath_algebra_real_matrix_get_item
        procedure, public, pass :: set_item => fmath_algebra_real_matrix_set_item
        procedure, public, pass :: get_submatrix => fmath_algebra_real_matrix_get_submatrix
        procedure, public, pass :: set_submatrix => fmath_algebra_real_matrix_set_submatrix
        procedure, public, pass :: add_value => fmath_algebra_real_matrix_add_value
        procedure, public, pass :: clone => fmath_algebra_real_matrix_clone
        procedure, public, pass :: to_transpose => fmath_algebra_real_matrix_transpose
        procedure, public, pass :: to_array => fmath_algebra_real_matrix_to_array
        procedure, public, pass :: to_transpose_array => fmath_algebra_real_matrix_to_transpose_array
        procedure, public, pass :: read_file => fmath_algebra_real_matrix_read_file
        procedure, public, pass :: write_file => fmath_algebra_real_matrix_write_file
        procedure, public, pass :: inverse => fmath_algebra_real_matrix_inverse
        procedure, public, pass :: solve => fmath_algebra_real_matrix_solve
    end type fmath_algebra_real_matrix_type

    interface new_fmath_algebra_real_matrix
        module procedure :: fmath_algebra_real_matrix_new1,&
                            fmath_algebra_real_matrix_new2,&
                            fmath_algebra_real_matrix_new3,&
                            fmath_algebra_real_matrix_new4,&
                            fmath_algebra_real_matrix_new5,&
                            fmath_algebra_real_matrix_new6
    end interface

    interface assignment (=)
        module procedure :: fmath_algebra_real_matrix_assign
    end interface

    interface operator (+)
        module procedure :: fmath_algebra_real_matrix_add
    end interface

    interface operator (-)
        module procedure :: fmath_algebra_real_matrix_negate
        module procedure :: fmath_algebra_real_matrix_subtract
    end interface

    interface operator (*)
        module procedure :: fmath_algebra_real_matrix_mult
    end interface

contains

    subroutine fmath_algebra_real_matrix_assign(other1, other2)
        class(fmath_algebra_real_matrix_type), intent(inout) :: other1
        class(fmath_algebra_real_matrix_type), intent(in) :: other2
        integer :: n, m, i, j
        n = other2%n_rows
        m = other2%n_cols
        other1%n_rows = n
        other1%n_cols = m
        if (allocated(other1%storage)) deallocate(other1%storage)
        allocate(other1%storage(n, m))
        do i = 1, n
            do j = 1, m
                other1%storage(i,j) = other2%storage(i,j)
            enddo
        enddo
        return
    end subroutine

    function fmath_algebra_real_matrix_add(other1, other2) result(res)
        use fmath_lapack_c
        type(fmath_algebra_real_matrix_type), intent(in) :: other1
        type(fmath_algebra_real_matrix_type), intent(in) :: other2
        type(fmath_algebra_real_matrix_type) :: res
        integer :: na, ma, nc, mc, i, j
        real :: alpha, beta
        real, dimension(:,:), allocatable :: A, B, C
        na = other1%n_rows
        ma = other1%n_cols
        nc = other2%n_rows
        mc = other2%n_cols
        res = new_fmath_algebra_real_matrix(0)
        if ((na .eq. nc) .and. (ma .eq. mc)) then
            alpha = 1.0
            beta = 1.0
            allocate(A(na, ma))
            allocate(B(ma, ma))
            allocate(C(na, ma))
            do i = 1, na
                do j = 1, ma
                    A(i,j) = other1%storage(i,j)
                enddo
            enddo
            B = 0.0
            do i = 1, ma
                B(i,i) = 1.0
            enddo
            do i = 1, nc
                do j = 1, mc
                    C(i,j) = other2%storage(i,j)
                enddo
            enddo
            call fmath_lapack_sgemm(alpha, A, beta, B, C)
            res = new_fmath_algebra_real_matrix(C)
        endif
        return
    end function

    function fmath_algebra_real_matrix_negate(other) result(res)
        type(fmath_algebra_real_matrix_type), intent(in) :: other
        type(fmath_algebra_real_matrix_type) :: res
        integer :: na, ma, i, j
        na = other%n_rows
        ma = other%n_cols
        res = new_fmath_algebra_real_matrix(na, ma)
        do i = 1, na
            do j = 1, ma
                res%storage(i,j) = -other%storage(i,j)
            enddo
        enddo
        return
    end function

    function fmath_algebra_real_matrix_subtract(other1, other2) result(res)
        use fmath_lapack_c
        type(fmath_algebra_real_matrix_type), intent(in) :: other1
        type(fmath_algebra_real_matrix_type), intent(in) :: other2
        type(fmath_algebra_real_matrix_type) :: res
        integer :: na, ma, nc, mc, i, j
        real :: alpha, beta
        real, dimension(:,:), allocatable :: A, B, C
        na = other1%n_rows
        ma = other1%n_cols
        nc = other2%n_rows
        mc = other2%n_cols
        res = new_fmath_algebra_real_matrix(0)
        if ((na .eq. nc) .and. (ma .eq. mc)) then
            alpha = 1.0
            beta = -1.0
            allocate(A(na, ma))
            allocate(B(ma, ma))
            allocate(C(na, ma))
            do i = 1, na
                do j = 1, ma
                    A(i,j) = other1%storage(i,j)
                enddo
            enddo
            B = 0.0
            do i = 1, ma
                B(i,i) = 1.0
            enddo
            do i = 1, nc
                do j = 1, mc
                    C(i,j) = other2%storage(i,j)
                enddo
            enddo
            call fmath_lapack_sgemm(alpha, A, beta, B, C)
            res = new_fmath_algebra_real_matrix(C)
        endif
        return
    end function

    function fmath_algebra_real_matrix_mult(other1, other2) result(res)
        use fmath_lapack_c
        type(fmath_algebra_real_matrix_type), intent(in) :: other1
        type(fmath_algebra_real_matrix_type), intent(in) :: other2
        type(fmath_algebra_real_matrix_type) :: res
        integer :: na, ma, nb, mb, i, j
        real :: alpha, beta
        real, dimension(:,:), allocatable :: A, B, C
        na = other1%n_rows
        ma = other1%n_cols
        nb = other2%n_rows
        mb = other2%n_cols
        res = new_fmath_algebra_real_matrix(0)
        if (ma .eq. nb) then
            alpha = 1.0
            beta = 0.0
            allocate(A(na, ma))
            allocate(B(nb, mb))
            allocate(C(na, mb))
            C = 0.0
            do i = 1, na
                do j = 1, ma
                    A(i,j) = other1%storage(i,j)
                enddo
            enddo
            do i = 1, nb
                do j = 1, mb
                    B(i,j) = other2%storage(i,j)
                enddo
            enddo
            call fmath_lapack_sgemm(alpha, A, beta, B, C)
            res = new_fmath_algebra_real_matrix(C)
        endif
        return
    end function

    function fmath_algebra_real_matrix_get_num_rows(self) result(res)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        integer :: res
        res = self%n_rows
        return
    end function

    function fmath_algebra_real_matrix_get_num_columns(self) result(res)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        integer :: res
        res = self%n_cols
        return
    end function

    function fmath_algebra_real_matrix_get_item(self, i, j) result(res)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        integer, intent(in) :: i, j
        real :: res
        res = self%storage(i, j)
        return
    end function

    subroutine fmath_algebra_real_matrix_set_item(self, i, j, va)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        integer, intent(in) :: i, j
        real, intent(in) :: va
        self%storage(i, j) = va
        return
    end subroutine

    function fmath_algebra_real_matrix_get_submatrix(self, row, n_rows, col, n_cols) result(res)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        integer, intent(in) :: row, n_rows, col, n_cols
        type(fmath_algebra_real_matrix_type) :: res
        integer :: i, j
        res%n_rows = n_rows
        res%n_cols = n_cols
        allocate(res%storage(n_rows,n_cols))
        do i = 1, n_rows
            do j = 1, n_cols
                res%storage(i, j) = self%storage(row + (i - 1), col + (j - 1))
            enddo
        enddo
        return
    end function

    subroutine fmath_algebra_real_matrix_set_submatrix(self, row, col, submatrix)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        integer, intent(in) :: row, col
        type(fmath_algebra_real_matrix_type), intent(inout) :: submatrix
        integer :: n_rows, n_cols
        integer :: i, j
        n_rows = submatrix%get_num_rows()
        n_cols = submatrix%get_num_columns()
        do i = 1, n_rows
            do j = 1, n_cols
                self%storage(row + (i - 1), col + (j - 1)) = submatrix%storage(i, j)
            enddo
        enddo
        return
    end subroutine

    function fmath_algebra_real_matrix_new1() result(this)
        type(fmath_algebra_real_matrix_type) :: this
        this%n_rows = 0
        this%n_cols = 0
        allocate(this%storage(0, 0))
        return
    end function

    function fmath_algebra_real_matrix_new2(n_rows, n_cols) result(this)
        integer, intent(in) :: n_rows, n_cols
        type(fmath_algebra_real_matrix_type) :: this
        this%n_rows = n_rows
        this%n_cols = n_cols
        if (allocated(this%storage)) deallocate(this%storage)
        allocate(this%storage(n_rows, n_cols))
        this%storage = 0.0
        return
    end function

    function fmath_algebra_real_matrix_new3(order) result(this)
        integer, intent(in) :: order
        type(fmath_algebra_real_matrix_type) :: this
        this = new_fmath_algebra_real_matrix(order, order)
        return
    end function

    function fmath_algebra_real_matrix_new4(source) result(this)
        type(fmath_algebra_real_matrix_type), intent(inout) :: source
        type(fmath_algebra_real_matrix_type) :: this
        integer :: i, j
        this%n_rows = source%get_num_rows()
        this%n_cols = source%get_num_columns()
        if (allocated(this%storage)) deallocate(this%storage)
        allocate(this%storage(this%n_rows, this%n_cols))
        do i = 1, this%n_rows
            do j = 1, this%n_cols
                this%storage(i, j) = source%get_item(i, j)
            enddo
        enddo
        return
    end function

    function fmath_algebra_real_matrix_new5(array) result(this)
        real, dimension(:,:), allocatable, intent(in) :: array
        type(fmath_algebra_real_matrix_type) :: this
        integer :: i, j
        this%n_rows = size(array, dim=1)
        this%n_cols = size(array, dim=2)
        if (allocated(this%storage)) deallocate(this%storage)
        allocate(this%storage(this%n_rows, this%n_cols))
        do i = 1, this%n_rows
            do j = 1, this%n_cols
                this%storage(i, j) = array(i, j)
            enddo
        enddo
        return
    end function

    function fmath_algebra_real_matrix_new6(array) result(this)
        real, dimension(:), allocatable, intent(in) :: array
        type(fmath_algebra_real_matrix_type) :: this
        integer :: i
        this%n_rows = size(array, dim=1)
        this%n_cols = 1
        allocate(this%storage(this%n_rows, this%n_cols))
        do i = 1, this%n_rows
            this%storage(i, 1) = array(i)
        enddo
        return
    end function

    function fmath_algebra_real_matrix_clone(self) result(res)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        type(fmath_algebra_real_matrix_type) :: res
        res = new_fmath_algebra_real_matrix(self)
        return
    end function

    subroutine fmath_algebra_real_matrix_add_value(self, i, j, va)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        integer, intent(in) :: i, j
        real, intent(in) :: va
        self%storage(i, j) = self%storage(i, j) + va
        return
    end subroutine

    function fmath_algebra_real_matrix_transpose(self) result(res)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        type(fmath_algebra_real_matrix_type) :: res
        integer :: i, j
        res = new_fmath_algebra_real_matrix(self%get_num_columns(), self%get_num_rows())
        do i = 1, self%get_num_rows()
            do j = 1, self%get_num_columns()
                call res%set_item(j, i, self%get_item(i, j))
            enddo
        enddo
        return
    end function

    function fmath_algebra_real_matrix_to_array(self) result(res)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        real, dimension(:,:), allocatable :: res
        integer :: i, j
        allocate(res(self%get_num_rows(), self%get_num_columns()))
        do i = 1, self%get_num_rows()
            do j = 1, self%get_num_columns()
                res(i, j) = self%get_item(i, j)
            enddo
        enddo
        return
    end function

    function fmath_algebra_real_matrix_to_transpose_array(self) result(res)
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        real, dimension(:,:), allocatable :: res
        type(fmath_algebra_real_matrix_type) :: aux
        allocate(res(self%get_num_columns(), self%get_num_rows()))
        aux = new_fmath_algebra_real_matrix(self%get_num_columns(), self%get_num_rows())
        aux = self%to_transpose()
        res = aux%to_array()
        return
    end function

    function fmath_algebra_real_matrix_read_file(self, file_path, header, position) result(res)
        use fio_text_file_class
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        character(len=fio_file_path_len), intent(in) :: file_path
        logical, optional, intent(in) :: header
        integer, optional, intent(in) :: position
        integer :: res
        integer :: i, j, n, m, ierr
        real :: aux
        type(fio_text_file_type) :: file
        res = 1
        file = new_fio_text_file_reader(file_path, ierr)
        if (present(header)) then
            if (header) then
                res = file%advance_line()
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
            endif
        endif
        if (present(position)) then
            do i = 1, position - 1
                res = file%advance_line()
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
            end do
        endif
        n = self%get_num_rows()
        m = self%get_num_columns()
        do i = 1, n
            do j = 1, m - 1
                res = file%read_real_with_separator(aux, .false.)
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
                call self%set_item(i, j, aux)
            enddo
            res = file%read_real(aux, .true.)
            if (res .ne. 0) then
                call file%to_close()
                return
            endif
            call self%set_item(i, j, aux)
        enddo
        call file%to_close()
        res = 0
        return
    end function

    function fmath_algebra_real_matrix_write_file(self, file_path, append, header) result(res)
        use fio_text_file_class
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        character(len=fio_file_path_len), intent(in) :: file_path
        logical, optional, intent(in) :: append
        logical, optional, intent(in) :: header
        integer :: res
        integer :: i, j, n, m, ierr
        real :: aux
        character(len=3) :: sn, sm
        character(len=fio_file_line_len) :: text
        type(fio_text_file_type) :: file
        res = 1
        if (present(append)) then
            file = new_fio_text_file_writer(file_path, ierr, append)
        else
            file = new_fio_text_file_writer(file_path, ierr, .false.)
        endif
        
        n = self%get_num_rows()
        m = self%get_num_columns()
        
        if (present(header) .and. header) then
            write(sn, '(A3)') n
            write(sm, '(A3)') m
            text = adjustl('Real matrix:'//adjustl(adjustr(sn)//'x'//adjustl(sm)))
            res = file%write_line(text)
        endif
        
        do i = 1, n
            do j = 1, m - 1
                aux = self%get_item(i, j)
                res = file%write_real_with_separator(aux, .false.)
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
            enddo
            aux = self%get_item(i, j)
            res = file%write_real(aux, .true.)
            if (res .ne. 0) then
                call file%to_close()
                return
            endif
        enddo
        call file%to_close()
        res = 0
        return
    end function

    function fmath_algebra_real_matrix_inverse(self) result(res)
        use fmath_algebra_inverse_c
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        type(fmath_algebra_real_matrix_type) :: res
        integer :: n, m
        real, dimension(:,:), allocatable :: a, inva
        n = self%get_num_rows()
        m = self%get_num_columns()
        allocate(a(n, m))
        allocate(inva(n, n))
        a = self%to_array()
        inva = fmath_algebra_inverse_real(a)
        res = new_fmath_algebra_real_matrix(inva)
        return
    end function

    function fmath_algebra_real_matrix_solve(self, b) result(res)
        use fmath_algebra_solve_c
        class(fmath_algebra_real_matrix_type), intent(inout) :: self
        type(fmath_algebra_real_matrix_type), intent(inout) :: b
        type(fmath_algebra_real_matrix_type) :: res
        integer :: na, ma, nb, mb
        real, dimension(:,:), allocatable :: aa, ba, xa
        na = self%get_num_rows()
        ma = self%get_num_columns()
        nb = b%get_num_rows()
        mb = b%get_num_columns()
        allocate(aa(na, ma))
        allocate(ba(nb, mb))
        allocate(xa(nb, mb))
        aa = self%to_array()
        ba = b%to_array()
        xa = fmath_algebra_solve_real(aa, ba)
        res = new_fmath_algebra_real_matrix(xa)
        return
    end function

end module fmath_algebra_real_matrix_c
