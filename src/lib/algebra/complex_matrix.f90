!
! Copyright (c) 2017 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
!****************************************************************************
!
! FILE: complex_matrix.f90
!
! AUTHOR: Casimiro Alvarez Mariño.
!
! CLASS: fmath_algebra_complex_matrix_c
!
! PURPOSE: clase que define matrices complejas de dos dimensiones.
!
! CREATED DATE: 2017/05/09
! MODIFIED DATE: 2017/06/01
!
!****************************************************************************
!
module fmath_algebra_complex_matrix_c
    implicit none
    private
    public :: fmath_algebra_complex_matrix_type, new_fmath_algebra_complex_matrix

    type fmath_algebra_complex_matrix_type
        integer :: n_rows
        integer :: n_cols
        complex, dimension(:,:), allocatable :: storage
    contains
        procedure, public :: get_num_rows => fmath_algebra_complex_matrix_get_num_rows
        procedure, public :: get_num_columns => fmath_algebra_complex_matrix_get_num_columns
        procedure, public :: get_item => fmath_algebra_complex_matrix_get_item
        procedure, public :: set_item => fmath_algebra_complex_matrix_set_item
        procedure, public :: get_submatrix => fmath_algebra_complex_matrix_get_submatrix
        procedure, public :: set_submatrix => fmath_algebra_complex_matrix_set_submatrix
        procedure, public :: add_value => fmath_algebra_complex_matrix_add_value
        procedure, public :: clone => fmath_algebra_complex_matrix_clone
        procedure, public :: to_transpose => fmath_algebra_complex_matrix_transpose
        procedure, public :: to_array => fmath_algebra_complex_matrix_to_array
        procedure, public :: to_array_part_real => fmath_algebra_complex_matrix_to_array_part_real
        procedure, public :: to_array_part_imag => fmath_algebra_complex_matrix_to_array_part_imag
        procedure, public :: to_transpose_array => fmath_algebra_complex_matrix_to_transpose_array
        procedure, public :: read_file => fmath_algebra_complex_matrix_read_file
        procedure, public :: write_file => fmath_algebra_complex_matrix_write_file
    end type fmath_algebra_complex_matrix_type

    interface new_fmath_algebra_complex_matrix
        module procedure :: fmath_algebra_complex_matrix_new1, &
                            fmath_algebra_complex_matrix_new2, &
                            fmath_algebra_complex_matrix_new3, &
                            fmath_algebra_complex_matrix_new4, &
                            fmath_algebra_complex_matrix_new5, &
                            fmath_algebra_complex_matrix_new6
    end interface

contains

    function fmath_algebra_complex_matrix_get_num_rows(self) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        integer :: res
        res = self%n_rows
        return
    end function

    function fmath_algebra_complex_matrix_get_num_columns(self) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        integer :: res
        res = self%n_cols
        return
    end function

    function fmath_algebra_complex_matrix_get_item(self, i, j) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        integer, intent(in) :: i, j
        complex :: res
        res = self%storage(i, j)
        return
    end function

    subroutine fmath_algebra_complex_matrix_set_item(self, i, j, value)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        integer, intent(in) :: i, j
        complex, intent(in) :: value
        self%storage(i, j) = value
        return
    end subroutine

    function fmath_algebra_complex_matrix_get_submatrix(self, row, n_rows, col, n_cols) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        integer, intent(in) :: row, n_rows, col, n_cols
        type(fmath_algebra_complex_matrix_type) :: res
        integer :: i, j
        res%n_rows = n_rows
        res%n_cols = n_cols
        allocate(res%storage(n_rows,n_cols))
        do i = 1, n_rows
            do j = 1, n_cols
                res%storage(i, j) = self%storage(row + (i - 1), col + (j - 1))
            enddo
        enddo
        return
    end function

    subroutine fmath_algebra_complex_matrix_set_submatrix(self, row, col, submatrix)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        integer, intent(in) :: row, col
        type(fmath_algebra_complex_matrix_type), intent(inout) :: submatrix
        integer :: n_rows, n_cols
        integer :: i, j
        n_rows = submatrix%get_num_rows()
        n_cols = submatrix%get_num_columns()
        do i = 1, n_rows
            do j = 1, n_cols
                self%storage(row + (i - 1), col + (j - 1)) = submatrix%storage(i, j)
            enddo
        enddo
        return
    end subroutine

    function fmath_algebra_complex_matrix_new1() result(this)
        type(fmath_algebra_complex_matrix_type) :: this
        this%n_rows= 0
        this%n_cols = 0
        allocate(this%storage(0, 0))
        return
    end function

    function fmath_algebra_complex_matrix_new2(n_rows, n_cols) result(this)
        integer, intent(in) :: n_rows, n_cols
        type(fmath_algebra_complex_matrix_type) :: this
        this%n_rows = n_rows
        this%n_cols = n_cols
        if (allocated(this%storage)) deallocate(this%storage)
        allocate(this%storage(n_rows, n_cols))
        this%storage = 0.0
        return
    end function

    function fmath_algebra_complex_matrix_new3(order) result(this)
        integer, intent(in) :: order
        type(fmath_algebra_complex_matrix_type) :: this
        this = new_fmath_algebra_complex_matrix(order, order)
        return
    end function

    function fmath_algebra_complex_matrix_new4(source) result(this)
        type(fmath_algebra_complex_matrix_type), intent(inout) :: source
        type(fmath_algebra_complex_matrix_type) :: this
        integer :: i, j
        this%n_rows = source%get_num_rows()
        this%n_cols = source%get_num_columns()
        if (allocated(this%storage)) deallocate(this%storage)
        allocate(this%storage(this%n_rows, this%n_cols))
        do i = 1, this%n_rows
            do j = 1, this%n_cols
                this%storage(i, j) = source%get_item(i, j)
            enddo
        enddo
        return
    end function

    function fmath_algebra_complex_matrix_new5(array) result(this)
        complex, dimension(:,:), allocatable, intent(in) :: array
        type(fmath_algebra_complex_matrix_type) :: this
        integer :: i, j
        this%n_rows = size(array, dim=1)
        this%n_cols = size(array, dim=2)
        if (allocated(this%storage)) deallocate(this%storage)
        allocate(this%storage(this%n_rows, this%n_cols))
        do i = 1, this%n_rows
            do j = 1, this%n_cols
                this%storage(i, j) = array(i, j)
            enddo
        enddo
        return
    end function

    function fmath_algebra_complex_matrix_new6(array) result(this)
        complex, dimension(:), allocatable, intent(in) :: array
        type(fmath_algebra_complex_matrix_type) :: this
        integer :: i
        this%n_rows = size(array, dim=1)
        this%n_cols = 1
        allocate(this%storage(this%n_rows, this%n_cols))
        do i = 1, this%n_rows
            this%storage(i, 1) = array(i)
        enddo
        return
    end function

    function fmath_algebra_complex_matrix_clone(self) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        type(fmath_algebra_complex_matrix_type) :: res
        res = new_fmath_algebra_complex_matrix(self)
        return
    end function

    subroutine fmath_algebra_complex_matrix_add_value(self, i, j, value)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        integer, intent(in) :: i, j
        complex, intent(in) :: value
        self%storage(i, j) = self%storage(i, j) + value
        return
    end subroutine

    function fmath_algebra_complex_matrix_transpose(self) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        type(fmath_algebra_complex_matrix_type) :: res
        integer :: i, j
        res = new_fmath_algebra_complex_matrix(self%get_num_columns(), self%get_num_rows())
        do i = 1, self%get_num_rows()
            do j = 1, self%get_num_columns()
                call res%set_item(j, i, self%get_item(i, j))
            enddo
        enddo
        return
    end function

    function fmath_algebra_complex_matrix_to_array(self) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        complex, dimension(:,:), allocatable :: res
        integer :: i, j
        allocate(res(self%get_num_rows(), self%get_num_columns()))
        do i = 1, self%get_num_rows()
            do j = 1, self%get_num_columns()
                res(i, j) = self%get_item(i, j)
            enddo
        enddo
        return
    end function

    function fmath_algebra_complex_matrix_to_array_part_real(self) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        real, dimension(:,:), allocatable :: res
        integer :: i, j
        allocate(res(self%get_num_rows(), self%get_num_columns()))
        do i = 1, self%get_num_rows()
            do j = 1, self%get_num_columns()
                res(i, j) = real(self%get_item(i, j))
            enddo
        enddo
        return
    end function

    function fmath_algebra_complex_matrix_to_array_part_imag(self) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        real, dimension(:,:), allocatable :: res
        integer :: i, j
        allocate(res(self%get_num_rows(), self%get_num_columns()))
        do i = 1, self%get_num_rows()
            do j = 1, self%get_num_columns()
                res(i, j) = imag(self%get_item(i, j))
            enddo
        enddo
        return
    end function

    function fmath_algebra_complex_matrix_to_transpose_array(self) result(res)
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        complex, dimension(:,:), allocatable :: res
        type(fmath_algebra_complex_matrix_type) :: aux
        allocate(res(self%get_num_columns(), self%get_num_rows()))
        aux = new_fmath_algebra_complex_matrix(self%get_num_columns(), self%get_num_rows())
        aux = self%to_transpose()
        res = aux%to_array()
        return
    end function

    function fmath_algebra_complex_matrix_read_file(self, file_path, header, position) result(res)
        use fio_text_file_class
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        character(len=fio_file_path_len), intent(in) :: file_path
        logical, optional, intent(in) :: header
        integer, optional, intent(in) :: position
        integer :: res
        integer :: i, j, n, m, ierr
        real :: a, b
        complex :: aux
        type(fio_text_file_type) :: file
        res = 1
        file = new_fio_text_file_reader(file_path, ierr)
        if (present(header)) then
            if (header) then
                res = file%advance_line()
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
            endif
        endif
        if (present(position)) then
            do i = 1, position - 1
                res = file%advance_line()
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
            enddo
        endif
        n = self%get_num_rows()
        m = self%get_num_columns()
        do i = 1, n
            do j = 1, m - 1
                res = file%read_real_with_separator(a, .false.)
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
                res = file%read_real_with_separator(b, .false.)
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
                aux = cmplx(a, b, kind = 4)
                call self%set_item(i, j, aux)
            enddo
            res = file%read_real_with_separator(a, .false.)
            if (res .ne. 0) then
                call file%to_close()
                return
            endif
            res = file%read_real(b, .true.)
            if (res .ne. 0) then
                call file%to_close()
                return
            endif
            aux = cmplx(a, b, kind = 4)
            call self%set_item(i, j, aux)
        enddo
        res = 0
        call file%to_close()
        return
    end function

    function fmath_algebra_complex_matrix_write_file(self, file_path, append, header) result(res)
        use fio_text_file_class
        class(fmath_algebra_complex_matrix_type), intent(inout) :: self
        character(len=fio_file_path_len), intent(in) :: file_path
        logical, optional, intent(in) :: append
        logical, optional, intent(in) :: header
        integer :: res
        integer :: i, j, n, m, ierr
        real :: a, b
        character(len=3) :: sn, sm
        character(len=fio_file_line_len) :: text
        type(fio_text_file_type) :: file
        res = 1
        if (present(append)) then
            file = new_fio_text_file_writer(file_path, ierr, append)
        else
            file = new_fio_text_file_writer(file_path, ierr, .false.)
        endif

        n = self%get_num_rows()
        m = self%get_num_columns()

        if (present(header) .and. header) then
            write(sn, '(A3)') n
            write(sm, '(A3)') m
            text = adjustl('Complex matrix:'//adjustl(adjustr(sn)//'x'//adjustl(sm)))
            res = file%write_line(text)
        endif

        do i = 1, n
            do j = 1, m - 1
                a = real(self%get_item(i, j))
                b = imag(self%get_item(i, j))
                res = file%write_real_with_separator(a, .false.)
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
                res = file%write_real_with_separator(b, .false.)
                if (res .ne. 0) then
                    call file%to_close()
                    return
                endif
            enddo
            a = real(self%get_item(i, j))
            b = imag(self%get_item(i, j))
            res = file%write_real_with_separator(a, .false.)
            if (res .ne. 0) then
                call file%to_close()
                return
            endif
            res = file%write_real(b, .true.)
            if (res .ne. 0) then
                call file%to_close()
                return
            endif
        enddo
        call file%to_close()
        res = 0
        return
    end function
end module fmath_algebra_complex_matrix_c
