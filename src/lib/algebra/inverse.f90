!
! Copyright (c) 2017 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
!****************************************************************************
!
! FILE: inverse.f90
!
! AUTHOR: Casimiro Alvarez Mariño.
!    
! CLASS: fmath_algebra_inverse_c
!
! PURPOSE: clase que define la inversa de una matriz.
!
! CREATED DATE: 2017/05/10
! MODIFIED DATE: 2017/05/10
!
!****************************************************************************
!
module fmath_algebra_inverse_c
    implicit none
    public

contains

    function fmath_algebra_inverse_real(a) result(res)
        use fmath_lapack_c
        real, dimension(:,:), allocatable, intent(in) :: a
        real, dimension(:,:), allocatable :: res
        integer :: n, m, i, info
        real, dimension(:,:), allocatable :: b
        n = size(a, dim=1)
        m = size(a, dim=2)
        allocate(res(n, n))
        if (n .eq. m) then
            allocate(b(n, n))
            b = 0.0
            do i = 1, n
                b(i,i) = 1.0
            end do
            call fmath_lapack_sgesv(n, n, a, b, res, info)
            if (info .ne. 0) then
                res = 0.0
            endif
        else
            res = 0.0
        endif
        return
    end function

    function fmath_algebra_inverse_complex(a) result(res)
        use fmath_lapack_c
        complex, dimension(:,:), allocatable, intent(in) :: a
        complex, dimension(:,:), allocatable :: res
        integer :: n, m, i, info
        complex, dimension(:,:), allocatable :: b
        n = size(a, dim=1)
        m = size(a, dim=2)
        allocate(res(n, n))
        if (n .eq. m) then
            allocate(b(n, n))
            b = (0.0, 0.0)
            do i = 1, n
                b(i,i) = cmplx(1.0, 0.0, kind = 4)
            end do
            call fmath_lapack_cgesv(n, n, a, b, res, info)
            if (info .ne. 0) then
                res = (0.0, 0.0)
            endif
        else
            res = (0.0, 0.0)
        endif
        return
    end function
    
end module fmath_algebra_inverse_c