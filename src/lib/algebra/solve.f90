!
! Copyright (c) 2017 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
!****************************************************************************
!
! FILE: solve.f90
!
! AUTHOR: Casimiro Alvarez Mariño.
!    
! CLASS: fmath_algebra_solve_c
!
! PURPOSE: clase para resolver un sistema de ecuaciones lineales.
!
! CREATED DATE: 2017/05/10
! MODIFIED DATE: 2017/05/10
!
!****************************************************************************
!
module fmath_algebra_solve_c
    implicit none
    public

contains

    function fmath_algebra_solve_real(a, b) result(res)
        use fmath_lapack_c
        real, dimension(:,:), allocatable, intent(in) :: a, b
        real, dimension(:,:), allocatable :: res
        integer :: na, ma, nb, mb, info
        na = size(a, dim=1)
        ma = size(a, dim=2)
        nb = size(b, dim=1)
        mb = size(b, dim=2)
        allocate(res(nb, mb))
        if ((na .eq. ma) .and. (na .eq. nb)) then
            call fmath_lapack_sgesv(na, mb, a, b, res, info)
            if (info .ne. 0) then
                res = 0.0
            endif
        else
            res = 0.0
        endif
        return
    end function

    function fmath_algebra_solve_complex(a, b) result(res)
        use fmath_lapack_c
        complex, dimension(:,:), allocatable, intent(in) :: a, b
        complex, dimension(:,:), allocatable :: res
        integer :: na, ma, nb, mb, info
        na = size(a, dim=1)
        ma = size(a, dim=2)
        nb = size(b, dim=1)
        mb = size(b, dim=2)
        allocate(res(nb, mb))
        if ((na .eq. ma) .and. (na .eq. nb)) then
            call fmath_lapack_cgesv(na, mb, a, b, res, info)
            if (info .ne. 0) then
                res = (0.0, 0.0)
            endif
        else
            res = (0.0, 0.0)
        endif
        return
    end function

end module fmath_algebra_solve_c