!
! Copyright (c) 2017 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
!****************************************************************************
!
! FILE: malapack.f90
!
! AUTHOR: Casimiro Alvarez Mariño.
!    
! CLASS: fmath_algebra_lapack_c
!
! PURPOSE: clase estática que llama a las funciones de la librería Lapack.
!
! CREATED DATE: 2017/05/10
! MODIFIED DATE: 2017/05/10
!
!****************************************************************************
!
module fmath_lapack_c
    implicit none
    public

contains

    subroutine fmath_lapack_sgemm(alpha, A, beta, B, C)
        !
        !   C = alpha * A * B + beta * C
        !
        implicit none

        interface
            subroutine sgemm ( TRANSA, TRANSB, M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC )
                implicit none
                character*1 TRANSA, TRANSB
                integer M, N, K, LDA, LDB, LDC
                real ALPHA, BETA
                real A( LDA, * ), B( LDB, * ), C( LDC, * ) 
            end subroutine
        end interface
        real*4, intent(in) :: alpha, beta
        real*4, dimension(:,:), allocatable, intent(in) :: A, B
        real*4, dimension(:,:), allocatable, intent(inout) :: C
        
        character*1 transa, transb
        integer*4 :: m, k, n
        integer*4 :: lda, ldb, ldc
        
        
        m = size(A, dim=1)
        k = size(A, dim=2)
        n = size(B, dim=2)

        transa = 'N'
        transb = 'N'
        lda = m
        ldb = k
        ldc = m

        call sgemm(transa,transb,m,n,k,alpha,A,lda,B,ldb,beta,C,ldc)

        return
    end subroutine

    subroutine fmath_lapack_sgesv(n, m, A, B, X, info)

        implicit none
        
        interface
            subroutine sgesv(n, nrhs, A, lda, Ipiv, B, ldb, info)
                implicit none
                integer info, lda, ldb, n, nrhs
                integer Ipiv( * )
                real A( lda, * ), B( ldb, * )
            end subroutine sgesv
        end interface
        
        integer, intent(in) :: n, m
        real, intent(in) :: A(n,n)
        real, intent(in) :: B(n,m)
        real, intent(inout) :: X(n,m)
        integer, intent(inout) :: info
        
        integer(4) :: i, j
        real, dimension(:,:), allocatable :: Baux
        integer(4), dimension(:), allocatable :: Ipiv

        allocate(Baux(n,m))
        allocate(Ipiv(n))
        
        Ipiv = 0
        do i = 1, n
            do j = 1, m
                Baux(i,j) = B(i,j)
            enddo
        enddo
    
        call sgesv(n,m,A,n,Ipiv,Baux,n,info)

        if (info .eq. 0) then
            do i = 1, n
                do j = 1, m
                    X(i,j) = Baux(i,j)
                enddo
            enddo
        endif
        
        return
    end subroutine

    subroutine fmath_lapack_dgesv(n, m, A, B, X, info)

        implicit none
        
        interface
            subroutine dgesv(n, nrhs, A, lda, Ipiv, B, ldb, info)
                implicit none
                integer info, lda, ldb, n, nrhs
                integer Ipiv( * )
                double precision A( lda, * ), B( ldb, * )
            end subroutine dgesv
        end interface
        
        integer, intent(in) :: n, m
        double precision, intent(in) :: A(n,n)
        double precision, intent(in) :: B(n,n)
        double precision, intent(inout) :: X(n,n)
        integer, intent(inout) :: info
        
        integer(4) :: i, j
        double precision, dimension(:,:), allocatable :: Baux
        integer(4), dimension(:), allocatable :: Ipiv

        allocate(Baux(n,m))
        allocate(Ipiv(n))
        
        do i = 1, n
            do j = 1, m
                Baux(i,j) = B(i,j)
            enddo
            Ipiv(i) = 0
        enddo
    
        call dgesv(n,m,A,n,Ipiv,Baux,n,info)

        if (info .eq. 0) then
            do i = 1, n
                do j = 1, m
                    X(i,j) = Baux(i,j)
                enddo
            enddo
        endif
        
        return
    end subroutine

    subroutine fmath_lapack_cgesv(n, m, A, B, X, info)

        implicit none
        
        interface
            subroutine cgesv(n, nrhs, A, lda, Ipiv, B, ldb, info)
                implicit none
                integer info, lda, ldb, n, nrhs
                integer Ipiv( * )
                complex A( lda, * ), B( ldb, * )
            end subroutine cgesv
        end interface
        
        integer, intent(in) :: n, m
        complex, intent(in) :: A(n,n)
        complex, intent(in) :: B(n,n)
        complex, intent(inout) :: X(n,n)
        integer, intent(inout) :: info
        
        integer(4) :: i, j
        complex, dimension(:,:), allocatable :: Baux
        integer(4), dimension(:), allocatable :: Ipiv

        allocate(Baux(n,m))
        allocate(Ipiv(n))
        
        do i = 1, n
            do j = 1, m
                Baux(i,j) = B(i,j)
            enddo
            Ipiv(i) = 0
        enddo
    
        call cgesv(n,m,A,n,Ipiv,Baux,n,info)

        if (info .eq. 0) then
            do i = 1, n
                do j = 1, m
                    X(i,j) = Baux(i,j)
                enddo
            enddo
        endif
        
        return
    end subroutine
    
    subroutine fmath_lapack_sgeev(n, A, W, VR, info)

        implicit none
    
        interface         
            subroutine sgeev(JOBVL,JOBVR,N,A,LDA,WR,WI,VL,LDVL,VR,LDVR,WORK,LWORK,INFO)
                implicit none
                character*1 JOBVL, JOBVR
		        integer N, LDA, LDVL, LDVR, LWORK, INFO
		        real, dimension( lda, * ) :: A
            	real, dimension( * ) :: WR, WI
		        real, dimension( ldvl, * ) :: VL
		        real, dimension( ldvr, * ) :: VR
		        real, dimension( * ) :: WORK
            end subroutine sgeev
        end interface
        
        integer, intent(in) :: n
        real, intent(in) :: A(n,n)
        complex, intent(inout) :: W(n)
        real, intent(inout) :: VR(n,n)
        integer, intent(inout) :: info
        
        integer :: i, j
        real, dimension(:), allocatable :: Wr, Wi, WORK
        real, dimension(:,:), allocatable :: VL
        
        allocate(Wr(n))
        allocate(Wi(n))
        allocate(WORK(4*n))
        allocate(VL(n,n))
        
        Wr = (0.0d0, 0.0d0)
        Wi = (0.0d0, 0.0d0)
        WORK = (0.0d0, 0.0d0)
        VL = (0.0d0, 0.0d0)
        
        call sgeev('N','V',n,A,n,Wr,Wi,VL,n,VR,n,WORK,4*n,info)

        if (info .eq. 0) then
            do i = 1, n
                W(i) = cmplx(Wr(i), Wi(i), kind = 4)
            enddo
        else
            W = (0.0d0, 0.0d0)
            VR = (0.0d0, 0.0d0)
        endif    
        
        return
    end subroutine

    subroutine fmath_lapack_dgeev(n, A, W, VR, info)
        
        implicit none
        
        interface         
            subroutine dgeev(JOBVL,JOBVR,N,A,LDA,WR,WI,VL,LDVL,VR,LDVR,WORK,LWORK,INFO)
                implicit none
                character*1 JOBVL, JOBVR
		        integer N, LDA, LDVL, LDVR, LWORK, INFO
                double precision, dimension( lda, * ) :: A
            	double precision, dimension( * ) :: WR, WI
		        double precision, dimension( ldvl, * ) :: VL
		        double precision, dimension( ldvr, * ) :: VR
		        double precision, dimension( * ) :: WORK
            end subroutine dgeev
        end interface
        
        integer, intent(in) :: n
        double precision, intent(in) :: A(n,n)
        complex*16, intent(inout) :: W(n)
        double precision, intent(inout) :: VR(n,n)
        integer, intent(inout) :: info
        
        integer :: i, j
        double precision, dimension(:), allocatable :: Wr, Wi, WORK
        double precision, dimension(:,:), allocatable :: VL
        
        allocate(Wr(n))
        allocate(Wi(n))
        allocate(WORK(4*n))
        allocate(VL(n,n))

        Wr = (0.0d0, 0.0d0)
        Wi = (0.0d0, 0.0d0)
        WORK = (0.0d0, 0.0d0)
        VL = (0.0d0, 0.0d0)
        
        call dgeev('N','V',n,A,n,Wr,Wi,VL,n,VR,n,WORK,4*n,info)
        
        if (info .eq. 0) then
            do i = 1, n
                W(i) = cmplx(Wr(i), Wi(i), kind = 8)
            enddo
        else
            W = (0.0d0, 0.0d0)
            VR = (0.0d0, 0.0d0)
        endif

        return
    end subroutine

    subroutine fmath_lapack_cgeev(n, A, W, VR, info)

        implicit none
        
        interface         
            subroutine cgeev(JOBVL,JOBVR,N,A,LDA,W,VL,LDVL,VR,LDVR,WORK,LWORK,RWORK,INFO)
                implicit none
                character*1 JOBVL, JOBVR
		        integer N, LDA, LDVL, LDVR, LWORK, INFO
		        complex, dimension( lda, * ) :: A
            	complex, dimension( * ) :: W
		        complex, dimension( ldvl, * ) :: VL
		        complex, dimension( ldvr, * ) :: VR
		        complex, dimension( * ) :: WORK
		        real, dimension( * ) :: RWORK
            end subroutine cgeev
        end interface
        
        integer, intent(in) :: n
        complex, intent(in) :: A(n,n)
        complex, intent(inout) :: W(n)
        complex, intent(inout) :: VR(n,n)
        integer, intent(inout) :: info
        
        integer :: i, j
        real, dimension(:), allocatable :: RWORK
        complex, dimension(:), allocatable :: WORK
        complex, dimension(:,:), allocatable :: VL
        
        allocate(RWORK(2*n))
        allocate(WORK(3*n))
        allocate(VL(n,n))
        
        RWORK = (0.0d0, 0.0d0)
        WORK = (0.0d0, 0.0d0)
        VL = (0.0d0, 0.0d0)

        call cgeev('N','V',n,A,n,W,VL,n,VR,n,WORK,3*n,RWORK,info)
        
        if (info .ne. 0) then
            W = (0.0d0, 0.0d0)
            VR = (0.0d0, 0.0d0)
        endif        
        
        return
    end subroutine

    subroutine fmath_lapack_zgeev(n, A, W, VR, info)
        
        implicit none
        
        interface         
            subroutine zgeev(JOBVL,JOBVR,N,A,LDA,W,VL,LDVL,VR,LDVR,WORK,LWORK,RWORK,INFO)
                implicit none
                character*1 JOBVL, JOBVR
		        integer N, LDA, LDVL, LDVR, LWORK, INFO
		        complex*16, dimension( lda, * ) :: A
            	complex*16, dimension( * ) :: W
		        complex*16, dimension( ldvl, * ) :: VL
		        complex*16, dimension( ldvr, * ) :: VR
		        complex*16, dimension( * ) :: WORK
		        double precision, dimension( * ) :: RWORK
            end subroutine zgeev
        end interface
        
        integer, intent(in) :: n
        complex*16, intent(in) :: A(n,n)
        complex*16, intent(inout) :: W(n)
        complex*16, intent(inout) :: VR(n,n)
        integer, intent(inout) :: info
        
        integer :: i, j
        double precision, dimension(:), allocatable :: RWORK
        complex*16, dimension(:), allocatable :: WORK
        complex*16, dimension(:,:), allocatable :: VL
        
        allocate(RWORK(2*n))
        allocate(WORK(3*n))
        allocate(VL(n,n))
        
        RWORK = (0.0d0, 0.0d0)
        WORK = (0.0d0, 0.0d0)
        VL = (0.0d0, 0.0d0)
        
        call zgeev('N','V',n,A,n,W,VL,n,VR,n,WORK,3*n,RWORK,info)
        
        if (info .ne. 0) then
            W = (0.0d0, 0.0d0)
            VR = (0.0d0, 0.0d0)
        endif
        
        return
    end subroutine

end module fmath_lapack_c