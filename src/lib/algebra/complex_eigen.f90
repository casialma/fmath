!
! Copyright (c) 2017 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
!****************************************************************************
!
! FILE: complex_eigen.f90
!
! AUTHOR: Casimiro Alvarez Mariño.
!
! CLASS: fmath_algebra_complex_eigen_c
!
! PURPOSE: clase que define los autovalores y autovectores de una matriz
!          compleja.
!
! CREATED DATE: 2017/05/10
! MODIFIED DATE: 2017/05/10
!
!****************************************************************************
!
module fmath_algebra_complex_eigen_c
    use fmath_algebra_complex_matrix_c
    implicit none
    private
    public :: fmath_algebra_complex_eigen_type, new_fmath_algebra_complex_eigen

    type fmath_algebra_complex_eigen_type
        type(fmath_algebra_complex_matrix_type) :: eigenvalues
        type(fmath_algebra_complex_matrix_type) :: eigenvectors
    contains
        procedure, public, pass :: get_eigenvalues => fmath_algebra_complex_eigen_get_eigenvalues
        procedure, public, pass :: get_eigenvectors => fmath_algebra_complex_eigen_get_eigenvectors

        procedure, private, pass :: calculate => fmath_algebra_complex_eigen_calculate
    end type fmath_algebra_complex_eigen_type

    interface new_fmath_algebra_complex_eigen
        module procedure :: fmath_algebra_complex_eigen_new
    end interface

contains

    function fmath_algebra_complex_eigen_get_eigenvalues(self) result(res)
        class(fmath_algebra_complex_eigen_type), intent(inout) :: self
        type(fmath_algebra_complex_matrix_type) :: res
        res = self%eigenvalues
        return
    end function

    function fmath_algebra_complex_eigen_get_eigenvectors(self) result(res)
        class(fmath_algebra_complex_eigen_type), intent(inout) :: self
        type(fmath_algebra_complex_matrix_type) :: res
        res = self%eigenvectors
        return
    end function

    function fmath_algebra_complex_eigen_new(a) result(this)
        type(fmath_algebra_complex_matrix_type), intent(inout) :: a
        type(fmath_algebra_complex_eigen_type) :: this
        this%eigenvalues = new_fmath_algebra_complex_matrix(0)
        this%eigenvalues = new_fmath_algebra_complex_matrix(0)
        call this%calculate(a)
        return
    end function

    subroutine fmath_algebra_complex_eigen_calculate(self, a)
        use fmath_lapack_c
        class(fmath_algebra_complex_eigen_type), intent(inout) :: self
        type(fmath_algebra_complex_matrix_type), intent(inout) :: a
        integer :: n, m, info
        complex, dimension(:), allocatable :: w
        complex, dimension(:,:), allocatable :: aa, vr
        n = a%get_num_rows()
        m = a%get_num_columns()
        if (n .eq. m) then
            allocate(w(n))
            allocate(aa(n, n))
            allocate(vr(n, n))
            w = (0.0, 0.0)
            aa = a%to_array()
            vr = (0.0, 0.0)
            call fmath_lapack_cgeev(n, aa, w, vr, info)
            if (info .eq. 0) then
                self%eigenvalues = new_fmath_algebra_complex_matrix(w)
                self%eigenvectors = new_fmath_algebra_complex_matrix(vr)
            endif
        endif
        return
    end subroutine

end module fmath_algebra_complex_eigen_c
